#!/usr/bin/python
""" Parse STR according to a given pattern.
"""

#
# Nomenclatures
# -------------

# AAAG:3,GAAAGAAG:1,GAAA:3,A:1,GAAA:4,AAGA:1,AAAG:5,AAAAAGAA:1,AAAG:13,AA:1
# AAAG:3,GAAAGAAG,GAAA:3,A,GAAA:4,AAGA,AAAG:5,AAAAAGAA,AAAG:13,AA
# AAAG3GAAAGAAG1GAAA3A1GAAA4AAGA1AAAG5AAAAAGAA1AAAG13AA1

# [AAAG]3[GAAAGAAG]1[GAAA]3[A]1[GAAA]4[AAGA]1[AAAG]5[AAAAAGAA]1[AAAG]13[AA]1
# [AAAG]3[GAAAGAAG][GAAA]3[A][GAAA]4[AAGA][AAAG]5[AAAAAGAA][AAAG]13[AA]
# [AAAG]3GAAAGAAG[GAAA]3A[GAAA]4AAGA[AAAG]5AAAAAGAA[AAAG]13AA


# ######
# AAAG[3]GAAAGAAG[1]GAAA[3]A[1]GAAA[4]AAGA[1]AAAG[5]AAAAAGAA[1]AAAG[13]AA[1]
# s.strand.split(']') 
#

from __future__ import print_function

import sys
import os

from collections import defaultdict
import pprint
import string  # for string.maketrans

import time  # for running time status

import re
import pysam  # for human genome FASTA indexing and for working with BAM files
import numpy as np

from pyparsing import *

from Bio import SeqIO

###
### Globals
###
DEBUG = False 


###
### Functions and classes
###

def strNomFileReader(strNomFileName, genome_index, max_f_len=None): #, flank_up_len_set=None, flank_down_len_set=None):
    """ Function for parsing through a .sn (STR nomenclature) file and storing
        all necessary info into appropriate data structures.

        For each STR locus, get into a dict with key = STR locus name:
        genomic coordinates
        flanking regions
        ref STR + its Rev Complement
        rep info: rep length, allele subtract correction
        a list w nomenclature info: [STR pattern list]
        #flank_up_len_set: setting hw long the up flank should b? (nothing 2 do with a Python 'set' data struct)

        return: str_info dict, str_order list with STRs ordered as specified in the str_file
    """
    ## map from IDs used in the nomenclature file to dictionary key names used in the str_info dictionary
    meta_data_map = {
        "FU" : "flank_up_len",
        "FD" : "flank_down_len",
        "UL" : "unit_len",
        "SUB" : "allele_subtract",
        "STRAND" : "strand",
        "DIR" : "str_dir_fwd",
        "SH" : "str_shift",
        "FORM": "allele_formula",
        "RNG": "known_allele_range",
        "MISM": "mismatches",  # e.g. 1,2 for up flank 1 mismatch, down flank 2 mismatches
        "RSU": "rsu",
        "RSD": "rsd"
    }

    str_info = defaultdict(str)
    str_patterns = defaultdict(list)
    str_order = list()
    #snp_info = list()  # list of strings with SNP entries from dbSNP

    genome_index = pysam.Fastafile( genome_index )

    with open(strNomFileName) as str_file:
        for line in str_file:
            if line.startswith("#") or not line.strip():
                continue
            #if line.startswith("@"):  # SNP info
            #    snp_info.append(line[1:])  # discard '@'
            #    continue
            if line.startswith(">"):
                str_info_line = line.strip().split()

                str_name = str_info_line[0][1:]  # ignore '>'
                print(str_name)
                str_info[ str_name ] = defaultdict(str)
                str_order.append( str_name )

                chrom, start, end = re.split(':|-', str_info_line[1].split(';')[0])  # take the first interval
                start = int(start)
                end = int(end)

                meta_data = str_info_line[2].split(';')
                for md in meta_data:
                    #print(md)
                    md_id,md_val = md.split(':')
                    str_info[ str_name ][meta_data_map[md_id]] = md_val

                ## get flanks
                if max_f_len:
                    flank_up_len = flank_down_len = 50
                else:
                    flank_up_len = int(str_info[ str_name ]["flank_up_len"]) #if ( not flank_up_len_set ) else flank_up_len_set
                    flank_down_len = int(str_info[ str_name ]["flank_down_len"]) #if ( not flank_down_len_set ) else flank_down_len_set

                if (flank_up_len < 0):  # if < 0 we pick the flank from within the STR region (I do this for AMELX/Y)
                    flank_up = genome_index.fetch(chrom, start, start-flank_up_len).upper()
                    flank_down =  genome_index.fetch(chrom, end+flank_down_len, end).upper()
                else:
                    flank_up = genome_index.fetch(chrom, start-flank_up_len, start).upper()
                    flank_down = genome_index.fetch(chrom, end, end+flank_down_len).upper()
                    str_info[ str_name ]["seq"] = genome_index.fetch(chrom, start, end).upper()

                    if str_info[ str_name ]["strand"] == 'r':
                        flank_up, flank_down = revCompl(flank_down).upper(), revCompl(flank_up).upper()
                        str_info[ str_name ]["seq"] = revCompl( str_info[ str_name ]["seq"] )

                flank_up_rev = revCompl(flank_down).upper()
                flank_down_rev = revCompl(flank_up).upper()
                str_info[ str_name ]["seqRC"] = revCompl( str_info[ str_name ]["seq"] )

                ## The contents of the str_info dict:
                #str_info[ str_name ]["seq"] = genome_index.fetch(chrom, start, end).upper()
                #str_info[ str_name ]["seqRC"] = revCompl( str_info[ str_name ]["seq"] )
                str_info[ str_name ]["str_shift"] = int(str_info[ str_name ]["str_shift"])
                str_info[ str_name ]["str_dir_fwd"] = True if str_info[ str_name ]["str_dir_fwd"] == 'f' else False
                str_info[ str_name ]["flank_up"] = flank_up
                str_info[ str_name ]["flank_up_len"] = int(str_info[ str_name ]["flank_up_len"])
                str_info[ str_name ]["flank_up_rev"] = flank_up_rev
                str_info[ str_name ]["flank_down"] = flank_down
                str_info[ str_name ]["flank_down_len"] = int(str_info[ str_name ]["flank_down_len"])
                str_info[ str_name ]["flank_down_rev"] = flank_down_rev
                str_info[ str_name ]["unit_len"] = int(str_info[ str_name ]["unit_len"])
                #str_info[ str_name ]["strand"] =
                #str_info[ str_name ]["allele_subtract"] = allele_subtract
                str_info[ str_name ]["chrom"] = chrom
                str_info[ str_name ]["start"] = start
                str_info[ str_name ]["end"] = end

                #str_info[ str_name ]["allele_formula"] = 
                #str_info[ str_name ]["known_allele_range"] = 
                ud_mism = str_info[ str_name ].get("mismatches", None)
                str_info[ str_name ]["mismatches"] = None if ud_mism == None else tuple(int(i) for i in ud_mism.split(','))
                #assert len(str_info[ str_name ]["mismatches"]) == 2, "only 2 values allowed for flank mismatches in the .sn file for system " + str_name

                str_info[ str_name ]["rsu"] = str_info[ str_name ].get("rsu", None)
                str_info[ str_name ]["rsd"] = str_info[ str_name ].get("rsd", None)
                # if str_info[ str_name ]["strand"] == 'r':
                #     if str_info[ str_name ]["rsu"] != None:
                #         tmp = str_info[ str_name ]["rsu"]
                #         print(tmp)
                #         snp_ref = revCompl( tmp.split(',')[3] )
                #         snp_alt = revCompl( tmp.split(',')[4] )
                #         str_info[ str_name ]["rsu"] = ','.join( tmp.split(',')[0:3] + [snp_ref, snp_alt] )
                #     if str_info[ str_name ]["rsd"] != None:
                #         tmp = str_info[ str_name ]["rsd"]
                #         print(tmp)
                #         snp_ref = revCompl( tmp.split(',')[3] )
                #         snp_alt = revCompl( tmp.split(',')[4] )
                #         str_info[ str_name ]["rsd"] = ','.join( tmp.split(',')[0:3] + [snp_ref, snp_alt] )
            else:
                print(str_name, str_info[ str_name ]["str_dir_fwd"])
                strPat = line.strip().split()[0]  # ignore comments (everything separated from the seq by at least one space)
                strPat = strPat if str_info[ str_name ]["str_dir_fwd"] else revComplStrPat( strPat )
                strPat = shiftStrPat( strPat, str_info[ str_name ]["str_shift"] )

                # Deal with int ranges/lists by making a nomenclature for each integer in the range
                # At the moment, I assume there's at most *one* range/list per nomenclature.
                match = re.search('\d+-\d+', strPat)
                match2 = re.search('\d+,\d+', strPat)
                if match:
                    int_range_str = match.group()
                    low_int, high_int = [int(s) for s in int_range_str.split('-')]
                    l, r = re.compile('\d+-\d+').split(strPat)
                    int_range = range(int(low_int),int(high_int)+1)
                    for i in int_range:
                        new_pat = l + str(i) + r
                        str_patterns[ str_name ].append(new_pat)
                elif match2:
                    int_list_str = match2.group()
                    int_list = [int(s) for s in int_list_str.split(',')]
                    l, r = re.compile('\d+,\d+').split(strPat)
                    for i in int_list:
                        new_pat = l + str(i) + r
                        str_patterns[ str_name ].append(new_pat)
                else:
                    str_patterns[ str_name ].append(strPat)

    for str_name in str_patterns:
        str_info[ str_name ]["nomenclature"] = str_patterns[ str_name ]

    if True:  # print str_info
        for key in str_order:
            print( key )
            for key2 in str_info[key]:
                print( "\t", key2, str_info[key][key2] )
            print()

        print(str_order,'\n')
        print('\n')

    return (str_info, str_order)




class STR_Allele:
    def __init__(self, in_obj, rep_len=4):
        self.rep_len=rep_len
        if isinstance(in_obj, str):
            try:
                allele_float = float(in_obj)
                self.allele = in_obj
            except ValueError as x:
                self.allele = self._getAlleleFromStr(in_obj)
                if DEBUG: print("self.allele = ", self.allele)
        elif isinstance(in_obj, float):
            self.allele = self.fromDecimalToAllele(in_obj)
        else:
            print("Not a legal allele input")
            exit(2)

    def _getAlleleFromStr(self, str_seq):
        """ (int, int) -> str
        Given a STR region str_seq and a repeat length of rep_len, return the STR count used for profiling
        in the form 'c.r' or 'c' if r == 0
        """
        str_len = len(str_seq)
        if DEBUG: print(str_len)
        count = str_len // self.rep_len
        rem = str_len % self.rep_len
        result = '{0}.{1}'.format(count,rem) #if rem > 0 else str(count)
        return result

    def _fromAlleleToDecimal(self):
        """ E.g. '12.3' --> 132.75
        """
        n,r = self.allele.split('.')
        return float(n) + float(r)*0.25

    def fromDecimalToAllele(self, x):
        """ E.g. 12.75 --> '12.3'
        """
        n = int(x)
        d = int(round((x - np.floor(x)) / 0.25))
        allele_count = str(n) + '.' + str(d)
        return allele_count

    def __str__(self):
        count, rem = self.allele.split('.')
        return count if rem == '0' else self.allele

    def __float__(self):
        return self._fromAlleleToDecimal()

    def __add__(self, allele2):
        return STR_Allele( float(self) + float(allele2) )

    def __sub__(self, allele2):
        tmp = STR_Allele( float(self) - float(allele2) )
        return tmp



### TODO: Implement Nomenclature class that contains all nomenclature related functions below.
class STR_Nomenclature:
    """ Nomenclature class that contains all nomenclature related functions.
    """

    alphas = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
    nom_symbols = list('ACGTN')
    nom_rep_zero_or_one = list('?')
    nom_rep_zero_or_more = list('*')
    nom_rep_one_or_more = list('+' + alphas)

    def __init__(self, nom):
        assert validate_nomenclature(nom), '{} is not a valid STR nomenclature.'.format(nom)
        self.nom = nom
        

    def validate_nomenclature(nom):
        """ Given a nomenclature string, return True if it's a valid nomenclature.

        :param nom: str - nomenclature string
        :return valid_nom: boolean - True if it's a valid nomenclature
        """
        valid_nom = False
        lol = strPatToLol(nom)

        for l in lol:
            symb = list(l[0])
            rep = l[1]





def strPatToLol(strPat):
    """ STR pattern to List of Lists
        E.g.
             AAGA[4]ACGA[1]AAGA[3] --> [[AAGA,4], [ACGA,1], [AAGA,3]]
             AAGA[+]ACGA[?]AAGA[*] --> [[AAGA,'+'], [ACGA,'?'], [AAGA,'*']]
        Assumes that the pattern always ends with ']'
    """
    strPatLol = [ x.split('[') for x in strPat.rstrip(']').split(']') ]  # STR pattern list of lists
    return strPatLol


def lolToStrPat(strPatLol):
    """ opposite of strPatToLol
    """
    return ''.join([x[0]+"["+str(x[1])+"]" for x in strPatLol])


def expandStrPat(strPat):
    """ Expand a given strPat to the corresponding string.
        E.g.
            AAGA[4]ACGA[1]AAGA[3] --> AAGAAAGAAAGAAAGAACGAAAGAAAGAAAGA
    """
    strPatLol = strPatToLol(strPat)    
    return ''.join([x[0] * int(x[1]) for x in strPatLol])



# def accum(lis):
#     total = 0
#     for l in lis:
#         total += l
#         yield total

def cshift(seq, n):
    """ cyclic left shift of seq
        positive n => left shift
        negative n => right shift
    """
    n = n % len(seq)
    return seq[n:]+seq[:n]

def shiftStrPat(strPat, n):
    """ Cyclic shift a simple or compound *consensus* strPat by n, where n is a positive or negative int. (not for non-consensus STRs)

        AAGA[4]ACGA[1]AAGA[3] --1--> AGAA[4]CGAA[1]AGAA[3]
        AAGA[4]ACGA[1]AAGA[3] --2--> GAAA[4]GAAC[1]GAAA[3]
        AAGA[4]ACGA[1]AAGA[3] --3--> AAAG[4]AACG[1]AAAG[3]
    """
    if (not n):  # do nothing if n = 0
        return strPat.upper()

    # split on counts => list of STR seq blocks (ignore last 3 signs, which is just the count for the last repeat block [n])
    # AAGA[4]ACGA[1]AAGA[3] => AAGA[4]ACGA[1]AAGA => ['AAGA','ACGA','AAGA']
    loBlocks = re.split('\[.\]', strPat[:-3])
    # split on repeat blocks => corresponding list of count brackets (1st is an empty list)
    # AAGA[4]ACGA[1]AAGA[3] => [[],[4],[1],[3]] => [[4],[1],[3]]
    loCounts = re.split('[ACGT]+', strPat)[1:]
    #loPos = list( accum([len(x) for x in loBlocks]) )  # list of cumulative block lengths
    #loStPos = [0] + loPos[:-1]   # 
    loNewBlocks = [cshift(block,n) for block in loBlocks]  # shift each block individually TBD is this always ok?
    #loNewBlocks = [newPat[s:e] for s,e in zip(loStPos,loPos)]
    newStrPat = ''.join( [b+c for b,c in zip(loNewBlocks,loCounts)] )  # recreate the pattern: new shifted blocks, same counts
    return (newStrPat.upper())


def revCompl(dna):
    return ''.join([ dict(zip("ACGTN","TGCAN"))[x] for x in dna.upper()[::-1] ])
    #return dna.upper()[::-1].translate(string.maketrans('ACGT','TGCA'))


def revComplStrPat(strPat):
    """ return the reverse complement of an STR pattern, e.g.,
        AAGA[4]ACGA[1]AAGA[3] --> TCTT[3]TCGT[1]TCTT[3]
    """
    return lolToStrPat(
        [
            [ revCompl(x[0]), x[1] ]
                for x in strPatToLol(strPat)[::-1]
        ] )
  

def shift_genomic_coordinates(start, end, n):
    """
        n > 0: start
    """
    pass


###
### The parser class - uses pyparsing
###
class STR_Parser:
    """ Recognizes instances having a given nomenclature."""

    def __init__(self, nomencl, unit_len=4, allele_subtract="0.0"):
        self.nomencl = nomencl  # the nomenclature string
        self.unit_len = unit_len
        self.allele_subtract = allele_subtract
        # generate and get the pyparsing grammer for this nomenclature string
        self.str_grammar_complete, self.str_grammar_partial = self._nom_to_pyparsing(nomencl)
        #print( self.str_grammar )
        #print isinstance(self.str_grammar,ParseElement)
        #print isinstance(self.str_grammar,ParseExpression)

    def _nom_to_pyparsing(self,nomencl):
        """ given a nomenclature string,
            construct a pyparsing grammar
        """
        if DEBUG:print("_nom_to_pyparsing ", nomencl )
        nomencl_lol = strPatToLol(nomencl)
        if DEBUG:print(nomencl_lol)
        #str_gram = StringStart()  # match the pattern from the string start
        str_gram = None
        str_gram_partial = None

        # for each subpattern
        for i in range(len(nomencl_lol)):
            pat = nomencl_lol[i]
            if DEBUG:
                print("now")
                print(pat)

            ## NOT used yet.
            ## If there's a 'N[m]' in the **middle** (not at the start, we assume) of a nomenclature, look ahead for the specific part following it
            ## NB: Tis can only handle one internal 'N[m]' ...
            ## Note: Carmen's UnknownSTR nomenclature was split into two specific ones, so for her data this part isn't an issue anymore.
            if (pat[0] == 'N' and pat[1] in list('+' + alphas)):
                remeinding_pat = nomencl_lol[i+1:]
                remeinding_gram = reduce( lambda a,b: a + b, [self._make_ParseExpression(p) for p in remeinding_pat] )
                remeinding_gram_partial = reduce( lambda a,b: a ^ b, [self._make_ParseExpression(p) for p in remeinding_pat] )
                str_gram = str_gram + SkipTo(remeinding_gram).addParseAction(
                    lambda s,l,t: ("{0}[{1}]".format("N", len(t[0])), float(len(t[0]))*len(t[0][0])/self.unit_len, l ) ) + remeinding_gram
                str_gram_partial = str_gram ^ SkipTo(remeinding_gram).addParseAction(
                    lambda s,l,t: ("{0}[{1}]".format("N", len(t[0])), float(len(t[0]))*len(t[0][0])/self.unit_len, l ) ) ^ remeinding_gram_partial

                #str_gram_partial = str_gram ^ FollowedBy(remeinding_gram).addParseAction(
                #    lambda s,l,t: ("{0}[{1}]".format("N", len(t[0])), float(len(t[0]))*len(t[0][0])/self.unit_len, l ) ) ^ remeinding_gram_partial
                break
            
            # convert each subpattern to a pyparsing expression

            if (i == 0):
                str_gram = str_gram_partial = self._make_ParseExpression(pat)
            else: 
                ## Complete match grammar: all subpatterns must matches
                str_gram = str_gram + self._make_ParseExpression(pat)
                ## Partial parsing grammar: use longest match
                # If the pattern is N[n] for some fixed number n, then we use
                # the FollowedBy function, otherwise the N[n] might be used
                # excessively in order to give a very long parse expression.
                if pat[0] == 'N':
                    str_gram_partial = str_gram_partial ^ FollowedBy(self._make_ParseExpression(pat))
                else:
                    str_gram_partial = str_gram_partial ^ self._make_ParseExpression(pat)

            if DEBUG: print("......")

        if DEBUG: print("b......")
        
        # The *entire* input STR string must match the complete match grammar.
        str_gram = StringStart() + str_gram + StringEnd()

        return (str_gram, str_gram_partial)

    def _make_ParseExpression(self,pat):
        if DEBUG: print(pat)
        try:
            rep = int(pat[1])  # if an exact match is specified
        except:
            if (pat[1] == "_"): # Fixed intervening pattern that doesn't count to the allele count. These do not occur in the original 9 STRs.
                #return CloseMatch(pat[0]).setParseAction(
                #    lambda s,l,t: ("{0}[{1}]".format(t[0],len(t)), 0) )
                # s=sequence, l=locus (where the token starts in the sequence), t=token
                # t = list of list of tokens: t[0] = first list of identical tokens; t[0][0] = first instance of first list of tokens
                return Literal(pat[0]).addParseAction(
                    lambda s,l,t: ("{0}[{1}]".format(t[0],len(t)), 0, l) )

            # One or more of the pattern.
            # MNOPQ are used in formulas - not implemented yet - right now they work the same way as '+'
            elif (pat[1] in list('+' + alphas)):
                return Group(OneOrMore(Literal(pat[0]))).addParseAction(
                    lambda s,l,t: ("{0}[{1}]".format(t[0][0],len(t[0])), float(len(t[0]))*len(t[0][0])/self.unit_len, l ) )
            elif (pat[1] == "*"):  # Zero or more of the pattern
                return Group(ZeroOrMore(Literal(pat[0]))).addParseAction(
                    lambda s,l,t: ("{0}[{1}]".format(t[0][0],len(t[0])), float(len(t[0]))*len(t[0][0])/self.unit_len, l ) )
            else:
                print("Hmmm... somthing wrong.")
                if DEBUG: print(pat)
                sys.exit(2)

        ## Fixed number of bases without regard to any type of pattern.
        if pat[0] == 'N':
            return Word('ACGT', exact=rep).addParseAction(
                    #lambda s,l,t: ("{0}[{1}]".format("N", len(t[0])), float(len(t[0]))*len(t[0][0])/self.unit_len, l ) )
                    lambda s,l,t: ("{0}[{1}]".format(t[0], 1), float(len(t[0]))*len(t[0][0])/self.unit_len, l ) )

        ## Fixed number of repeats of this pattern.
        ## pat[0] is the pattern, and t[0] = pat[0] * rep is the matched token
        return Literal( pat[0] * rep ).addParseAction(
            lambda s,l,t: ("{0}[{1}]".format(pat[0],rep), float(len(pat[0])*rep)/self.unit_len, l) )

    def parse_STR(self,str_sequence):
        try:
            #if DEBUG:
            if DEBUG: print("Attempting complete string parsing.")
            if DEBUG: print("Complete grammar: ", self.str_grammar_complete)
            parse_result = self.str_grammar_complete.parseString(str_sequence)
            parse_result_nom = ""
            allele_num = 0
            if DEBUG: print(parse_result)
            if DEBUG: print(parse_result)

            for m in parse_result:
                if DEBUG: print(m)
                parse_result_nom += m[0]
                allele_num += m[1]

            if DEBUG: print(parse_result_nom)
            # if a SUB is specified in the .sn file
            allele_num = STR_Allele(allele_num) - STR_Allele(self.allele_subtract)
            return (True, parse_result_nom, allele_num )

        except ParseException as x:
            try:
                if DEBUG: print("Attempting partial string parsing.")
                if DEBUG: print("Partial grammar: ", self.str_grammar_partial)
                ## From the pyparsing doc:
                ## "returns a list of the matching tokens returned from each call to scanString"
                parse_result = self.str_grammar_partial.searchString(str_sequence)

                if len(parse_result) == 0:  # if no result, then no match
                    raise ParseException("Empty parse result.")

                parse_result_nom = ""
                parse_result_string = ""
                allele_num = 0
                if DEBUG: print("Orig: " + str_sequence)
                if DEBUG: print("Parse res: {}".format(parse_result))
                if DEBUG: print("PP gram: {}".format(self.str_grammar_partial))
                matched_orig_res_lol = originalTextFor(self.str_grammar_partial).searchString(str_sequence)
                if DEBUG: print("Matched orig res: {}".format(matched_orig_res_lol))
                
                for i in range(len(parse_result)):  # for each 'parse result unit' = pru
                    pru = parse_result[i]
                    if DEBUG: print(pru)
                    mru = matched_orig_res_lol[i]  # match result unit
                    if DEBUG: print(mru)
                    matched_string = mru[0]

                    if DEBUG: print("Matched seq: {}".format(matched_string))
                    l = pru[0][2]
                    if DEBUG: print("match start at: " + str(l))
                    if DEBUG: print(len(parse_result_string),l)
                    unparsed_seq = str_sequence[len(parse_result_string):l].lower()
                    parse_result_string += unparsed_seq
                    #unparsed_seq = unparsed_seq + "[1]" if unparsed_seq else unparsed_seq  # if not empty, add 'j[1]'
                    parse_result_nom += unparsed_seq 
                    if DEBUG: print("parse res: " + parse_result_nom)
                    if DEBUG: print("parse res: " + parse_result_string)

                    for m in pru:
                        parse_result_nom += m[0]
                        parse_result_string += expandStrPat( m[0] )
                        allele_num += m[1]
                        if DEBUG: print("parse res: " + parse_result_nom)
                    
                unparsed_seq = str_sequence[l+len(matched_string):].lower()
                #unparsed_seq = unparsed_seq + "[1]" if unparsed_seq else unparsed_seq  # if not empty, add 'j[1]'
                parse_result_nom += unparsed_seq
                if DEBUG: print("parse res: " + parse_result_nom)

                # I don't use the above computed allele_num here
                if DEBUG: print("as ", STR_Allele(str_sequence, self.unit_len))
                if DEBUG: print("sub ", STR_Allele(self.allele_subtract))
                allele_num = STR_Allele(str_sequence, self.unit_len) - STR_Allele(self.allele_subtract)
                if DEBUG: print("an ", allele_num)
                return (False, parse_result_nom, allele_num )

            except ParseException as x:  # allele number is not exactly this always, as the BED file might specify a number to b subracted
                if DEBUG: print("No match")
                allele_num = STR_Allele(str_sequence, self.unit_len) - STR_Allele(self.allele_subtract)
                return (False, str_sequence.lower(), allele_num )



def getFSA(nom_list, unit_len, allele_subtract, allele_formula):
    """
    Create and return Finite State Automaton (FSA) that accepts or rejects
    strings based on list of Nomenclatures.

    nom_list : nomenclatures defined in .sn file
    unit_len : length of repeat
    allele_subtract : Value to subtract from allele profile, eg 6.3 - 2.1 = 4.2
    allele_formula : NOTUSED
    """
    if DEBUG: print("getFSA a")
    fsa_list = [STR_Parser(nom, unit_len, allele_subtract) for nom in nom_list]
    if DEBUG: print("getFSA b")
    def fsa(str_sequence):
        if DEBUG: print("Running FSAs. # of FSAs: " + str(len(fsa_list)))
        accept_list = list()
        i = 0
        for f in fsa_list:
            i += 1
            if DEBUG: print("testing nom #{}".format(i))
            (accept,matchPat,allele_num) = f.parse_STR(str_sequence)
            if DEBUG: print("ii")
            accept_list.append( (accept,matchPat,allele_num) )
            if DEBUG: print("iu")
            if accept:
                if DEBUG: print("accepted")
                return (accept,matchPat,allele_num)

        # which parse is the best if there's more than one and none of them are complete parses of the str_sequence?
        # 1. parse w the longest allele # is better than one w a shorter one
        # 2. if there r > 1 w the same longest allele #, then we pick the one w the fewest lower case letters = the one w the most uppercase letters = the one w the most '['s
        #sorted_accept_list = sorted( accept_list, key= lambda x : len(x[1]))
        sorted_accept_list = sorted( accept_list, key = lambda x: float(x[2]), reverse=True)  # 1.
        if DEBUG: print(sorted_accept_list)
        if len(sorted_accept_list) > 0:
            if DEBUG: print("aa")
            max_allele_number = sorted_accept_list[0][2]
            if DEBUG: print("ab")
            sorted_filtered_accept_list = filter( lambda x: x[2] == max_allele_number, sorted_accept_list ) # parses w max allele #
            if DEBUG: print("ac")
            final_list = sorted( sorted_filtered_accept_list, key = lambda x: x[1].count('['), reverse=True) # 2.
            if DEBUG: print("ad")
            if len(final_list) > 0:
                if DEBUG: print(final_list)
                if DEBUG: print()
                return final_list[0]
        else:
            return (False,"",STR_Allele("0.0"))

    return fsa



###
### main module test
###

# test of FSA
def testSTR(str_sequence, strPatList, unit_len, allele_subtract, allele_formula):
    fsa = getFSA(strPatList,unit_len,allele_subtract, allele_formula)
    return fsa(str_sequence)



def testParser(strNomFileName, str_test_sequences):
    print( "Time:", time.strftime("%H:%M:%S") )
    genome_index = "/mnt/NGS01/Homo_Sapiens/UCSC/hg19/Sequence/WholeGenomeFASTA/WholeGenomeFASTA.fa"
    str_info = strNomFileReader(strNomFileName, genome_index)[0]

    for seq_record in SeqIO.parse(str_test_sequences, "fasta"):
        print("***")
        print( seq_record.description )
        seq = str(seq_record.seq.upper())
        print( seq )
        print("sub = ", str_info[seq_record.id]['allele_subtract'])
        accept, nom, allele_num = testSTR(seq, str_info[seq_record.id]['nomenclature'], str_info[seq_record.id]['unit_len'], str_info[seq_record.id]['allele_subtract'], str_info[seq_record.id]['allele_formula'])
        print(accept,nom,allele_num)
        print( "Accept {0}\nNom {1}\nAllele {2}\n".format(accept,nom,allele_num) )
        print()



def test_STR_Allele():
    a = STR_Allele("11.3")
    b = STR_Allele("3.3")

    print( a )
    print( a + b )
    print( a - b )
    print( float(a))
    print( float(b))

    c = STR_Allele(12.25)
    print( c )

    d = STR_Allele("AATGAATGAATGAATGATGAATGAATG")
    print( d )



if __name__ == '__main__':
    #strNomFileName = "../conf/STR_nomDB_v1.sn"
    #str_test_sequences = "../conf/STR_test_sequences_usualSTRs.fa"
    #testParser(strNomFileName,str_test_sequences)

    # strNomFileName = "../conf/STR_XBlock.sn"
    # str_test_sequences = "../conf/STR_test_sequences_XBlock.fa"
    # testParser(strNomFileName,str_test_sequences)

    strNomFileName = "../conf/STR_Ychr_new.sn"
    str_test_sequences = "../conf/STR_test_sequences_YChr.fa"
    testParser(strNomFileName,str_test_sequences)


    #test_STR_Allele()
