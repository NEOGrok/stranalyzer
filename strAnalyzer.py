#!/bin/env python
""" Given a fastq sample file, split up into STR loci based on their unique flanking regions (using agrep)
    and determine the STR alleles (the profile counts).


    Usage:
        strAnalyzer -x <index> -n <str_nom> -i <data_file> [-v <vcf_file>] [-m <flank_mism>] [-o <outdir>] [-p <n_processes>] [-f <max_f_len>]
        strAnalyzer -h | --help
        strAnalyzer --version

    Options:
        -h --help                           Show this screen.
        --version                           Show version.

        -x --index <index>                  Reference genome FASTA file
        -n --str-nom-file <str_nom>         file with STR nomenclature and reference STR position info
        -v --vcf-file <vcf_file>            VCF file with e.g. dbSNP
                                            Used if no SNP info is specified in <str-nom-file>
        -i --in <data_file>                 Input file: FASTQ, FASTQ.gz, BAM file with RG info
        -m --flank-mism <flank_mism>        Number of mismatches (indel/substitutions) allowed in each flanking region.
                                            NB: So far only implemented to deal with at most 1 nucleotide indel or substitution (i.e. 1 mismatch)
                                            [default: 0]
        -o --outdir <outdir>                (Create and) output to this subdirectory
                                            (default: <data_file>_M<flank_mism>)
        -p --n-processes <n_processes>      How many processes should be spawned? [default: 1]
        -f --flank-len-optim <max_f_len>    Run flank length optimization from 5 to <max_f_len>. The default means no optimization.
                                            [default: 0]

    Output: the following files are written to <outdir>:
        locusName_wFlanks.fasta = fasta file w. flanking regions for each STR locus
        locusName_woFlanks.fasta = fasta file w.o. flanking regions for each STR locus
        STR_total_count.results = file with # sequences for each allele of each locus
        STR_seq_count.results = file with # sequences for each unique (lflank,STR,rflank) >1% of total, sorted according to # sequences
        STR_seq_count_woFlanks.results = file with # sequences for each unique (lflank,STR,rflank) >1% of total, sorted according to # sequences
        STR_alleleResultsPrediction_wFlanks.results
        STR_alleleResultsPrediction_woFlanks.Results
"""

from __future__ import print_function

### own modules
import strtools
from strtools import STR_Allele

#from flankLengthOptim import *

### other modules
#import regex
import re
import docopt
import sys
import os
import commands
import time
from collections import defaultdict, Counter
import itertools
import math
#import pdb  # Py debugger

from Bio import pairwise2
#import difflib
import pysam  # for human genome FASTA indexing and for working with BAM files
#from pybedtools import BedTool

# for plotting
import numpy as np
import matplotlib.pyplot as plt  # use the stateful interface to matplotlib
import matplotlib.ticker as ticker  # use the stateful interface to matplotlib
from matplotlib.backends.backend_pdf import PdfPages  # PDF backend (output device)
import mpl_toolkits.axisartist as AA
from mpl_toolkits.mplot3d import Axes3D  # 3D plotting for flank length optimization


# storing data
try:
    import cPickle as pickle  # faster
except:
    import pickle

# parallel execution
import multiprocessing as mp


###
### Global "constants", i.e., do not modify them anywhere.
DEBUG = False

__version__ = 'v1.0.0'





def startupMessage():
    print()
    print("###")
    print("### STR Analyzer " + __version__ + ' ###')
    print("###")
    print()


def printTime(msg):
    print( "{}: {}\n".format(msg,time.strftime("%H:%M:%S")) )



def log(file, text, append = True):
    msg = "{}: {}\n".format(text,time.strftime("%H:%M:%S"))

    with open(file, 'a' if append else 'w') as logFile:
        logFile.write(msg)

    print( msg )


def log_run_params(log_file, log_dic):
    for k,v in log_dic.iteritems():
        log(log_file, k + " : " + str(v) + "\n")


def processCommandLineParams():
    ### get arguments
    arguments = docopt.docopt(__doc__, version=__version__)
    if DEBUG: print( arguments )
    #exit(0)

    genome_index = arguments['--index']
    #str_file = arguments['--str-file']  # info moved to str-nom-file
    str_nom_file = arguments['--str-nom-file']
    vcf_file = arguments['--vcf-file']
    data_file = arguments['--in']
    flank_mism = int(arguments['--flank-mism'])
    outdir = arguments['--outdir']
    n_processes = int(arguments['--n-processes'])
    max_f_len = int(arguments['--flank-len-optim'])

    ### process arguments
    (base_name, ext) = os.path.splitext( os.path.basename(data_file) )  # remove outer .bam, .fastq or .gz extension
    if not outdir:
        outdir = 'STR_' + baseName + '_M' + str(flank_mism)
    if not os.path.exists( outdir ):
        os.makedirs( outdir )
    if not os.path.isfile( str_nom_file ):
        print( "STR nomenclature file does not exist." )
        sys.exit(2)

    if (ext == ".gz"):
        base_name = os.path.splitext( base_name )[0]  # remove .fastq/.bam extension
        unzipped_fastq = os.path.join( outdir, base_name + ".fastq" )  # unzipped name
        if not os.path.isfile( unzipped_fastq ):
            print( "Unzipping {0} ...".format(data_file) )
            os.system( "gunzip -c " + data_file + " > " + unzipped_fastq )

        data_file = unzipped_fastq

    # parameters = {}
    # parameters['data_file'] = data_file
    # parameters['str_nom_file'] = str_nom_file
    # parameters['genome_index'] = genome_index
    # parameters['flank_mism'] = flank_mism
    # parameters['outdir'] = outdir
    # parameters['vcf_file'] = vcf_file
    # parameters['n_processes'] = n_processes
    # parameters['max_f_len'] = max_f_len
    # return parameters
    return (data_file,str_nom_file,genome_index,flank_mism,outdir,vcf_file,n_processes,max_f_len)




### Flank length optimization functions

def flank_length_optim_worker(more_args):
    STR = more_args[0]
    outdir = more_args[1]['outdir']
    str_info = more_args[1]['srt_info']
    seq_count_file_name = more_args[1]['seq_count_file_name']
    flank_mism = more_args[1]['flank_mism']
    min_flank_length = more_args[1]['min_flank_length']
    max_flank_length = more_args[1]['max_flank_length']

    def optimize_flank(up_flank=True):
        """ up_flank = True: run sweep on up flank
            up_flank = False: run sweep on down flank
        """
        print("optimize_flank: {} {}".format(min_flank_length,max_flank_length))
        flank_len_opt_results = list()
        for flank_length in range(min_flank_length,max_flank_length):
            dummy_str_filename = os.path.join(outdir, STR + '.str')

            if (up_flank):
                print("up_flank_length" + str(flank_length))
                findSTRRegions(seq_count_file_name, dummy_str_filename, str_info, STR, flank_mism, flank_length_up=flank_length, flank_length_down=15)
            else:
                print("down_flank_length" + str(flank_length))
                findSTRRegions(seq_count_file_name, dummy_str_filename, str_info, STR, flank_mism, flank_length_up=15, flank_length_down=flank_length)
            cmd = "awk -F ':' '{n+=$4} END{print n}' " + dummy_str_filename
            (status, tot_seq) = commands.getstatusoutput(cmd)
            if status:
                print( "error" )
                sys.exit(1)
            else:
                print('{0} total reads in {1}'.format(tot_seq,dummy_str_filename) )
                flank_len_opt_results.append( tot_seq )

        return flank_len_opt_results

    opt_result_u = optimize_flank( up_flank = True )
    opt_result_d = optimize_flank( up_flank = False )
    
    return (STR, opt_result_u, opt_result_d)



def flank_length_optim_exhaustive_worker(more_args):
    STR = more_args[0]
    outdir = more_args[1]['outdir']
    str_info = more_args[1]['srt_info']
    seq_count_file_name = more_args[1]['seq_count_file_name']
    flank_mism = more_args[1]['flank_mism']
    min_flank_length = more_args[1]['min_flank_length']
    max_flank_length = more_args[1]['max_flank_length']
   
    opt_result_mx = np.zeros( (max_flank_length - min_flank_length + 1,max_flank_length - min_flank_length + 1) )
    for (x,y), value in np.ndenumerate(opt_result_mx):
        #if x < min_flank_length or y < min_flank_length:
        #    continue
        print(x,y,value)
        #if x + min_flank_length == 14 and y + min_flank_length > 20:
        #    break
        print("optimize_flank: {} {}".format(x,y))
        #flank_len_opt_results = list()
        #for flank_pair in range(min_flank_length,max_flank_length):
        dummy_str_filename = os.path.join(outdir, STR + "_flank_len_opt_" + str(x) + str(y) + '.str')
        findSTRRegions(seq_count_file_name, dummy_str_filename, str_info, STR, flank_mism, flank_length_up=x+min_flank_length, flank_length_down=y+min_flank_length)
        cmd = "awk -F ':' '{n+=$4} END{print n}' " + dummy_str_filename
        (status, tot_seq) = commands.getstatusoutput(cmd)
        if status:
            print( "error" )
            sys.exit(1)
        else:
            print('{0} total reads in {1}'.format(tot_seq,dummy_str_filename) )
        opt_result_mx[x,y] = int(tot_seq) if tot_seq else 0
        os.remove(dummy_str_filename)

    return (STR, opt_result_mx)



def flank_length_optimization(min_flank_length, max_flank_length, seq_count_file_name, str_info, flank_mism, n_processes, exhaustive=False):
    print("FLO for " + seq_count_file_name)
    outdir = os.path.dirname(seq_count_file_name)
    opt_result_dict = {}
    pickle_file_name = os.path.join(outdir, 'FLU_exhaustive.pickle') if exhaustive else os.path.join(outdir, 'FLU.pickle')
    if not os.path.isfile( pickle_file_name ):
        pool = mp.Pool(processes=n_processes)
        more_args = {
            'outdir' : outdir,
            'srt_info' : str_info,
            'seq_count_file_name' : seq_count_file_name,
            'flank_mism' : flank_mism,
            'min_flank_length' : min_flank_length,
            'max_flank_length' : max_flank_length
        }

        if (exhaustive):
            results = pool.map(flank_length_optim_exhaustive_worker, itertools.izip(list(str_info.keys()), itertools.repeat(more_args)))
            for r in results:
                opt_result_dict[r[0]] = r[1]
        else:
            results = pool.map(flank_length_optim_worker, itertools.izip(list(str_info.keys()), itertools.repeat(more_args)))
            for r in results:
                opt_result_dict[r[0] + '_u'] = r[1]
                opt_result_dict[r[0] + '_d'] = r[2]

        with open(pickle_file_name, "wb") as pickleFile:
            try: # pickle results data
                pickle.dump( opt_result_dict, pickleFile )
            except pickle.PicklingError as e:
                print( e )
                sys.exit(2)
    else:
        with open(pickle_file_name, "rb") as pickleFile:
            opt_result_dict = pickle.load(pickleFile)


    i_ax = 0
    axis_label_fontSize = 4
    if (exhaustive):
        pp = PdfPages(os.path.join(outdir, 'flank_opt_results_exhaustive.pdf'))
        #subplots = [330 + i for i in range(1,10)]
        for key, mx in opt_result_dict.items():
            if key.startswith('AMEL'):
                continue
            print(mx)
            print(mx.max())
            fig = plt.figure()

            def plot_3d_fig_angle(ang, type):
                print("Plotting angle = {}, type = {}".format(ang,type))
                #ax = fig.add_subplot(subplots[i_ax], projection='3d')
                ax = Axes3D(fig) #fig.add_subplot(111, projection='3d')
                #ax = fig.add_subplot(111)
                xs = [s[0][0] + min_flank_length for s in np.ndenumerate(mx)]
                #print(xs)
                ys = [s[0][1] + min_flank_length for s in np.ndenumerate(mx)]
                zs = [s[1] for s in np.ndenumerate(mx)]
                if type == "scatter":
                    ax.scatter(xs, ys, zs, zdir='z', s=5, alpha=.9, c='red')
                elif type == "bar":
                    ax.bar(xs, zs, ys, zdir='y', alpha=1, color='red')
                else:
                    print("Unknown plot type (scatter or bar)")
                    sys.exit(2)
                #ax.view_init(30,90)
                ax.elev = 30
                ax.azim = ang
                #ax.plot(xs, ys, zs, markersize=5, alpha=.7)
                #ax.plot(range(mx.shape[0]), mx[:,min_flank_length], markersize=2, alpha=.5)
                ax.set_xlabel('up_flank', fontsize=5)
                ax.set_ylabel('down_flank', fontsize=5)
                #ax.plot(range(mx.shape[1]), mx[min_flank_length,:], markersize=2, alpha=.5)
                ax.set_zlabel('#', fontsize=5)
                ax.set_xlim(min_flank_length,max_flank_length)
                ax.set_ylim(min_flank_length,max_flank_length)
                ax.set_zlim(bottom=0)
                #ax.set_zlim(0,100000)
                [t.set_fontsize(axis_label_fontSize) for t in ax.get_xticklabels()]
                [t.set_fontsize(axis_label_fontSize) for t in ax.get_yticklabels()]
                [t.set_fontsize(axis_label_fontSize) for t in ax.get_zticklabels()]
                ax.set_title("{}_{}".format(key,ang), fontsize=6, fontweight="bold", va="center")
                #i_ax += 1
                pp.savefig(fig)

            for ang in [15,45,75]:
                plot_3d_fig_angle(ang, "bar")
                plot_3d_fig_angle(ang, "scatter")
        pp.close()
    else:
        df = DataFrame( opt_result_dict,
            columns = [opt_result_dict.keys()].sort(),
            index = range(min_flank_length,max_flank_length ) )
        #pp = PdfPages(os.path.join(outdir,"alleles_{0}.pdf".format(os.path.basename(self.outdir))))
        #num_STRs = len(str_info)   # how many STRs do we have. Used to determine hw many subplots to create.
        #subplot_n = -1   # current subplot number; incremented when plotting results for a new STR
        i_df = -1  # data frame index for getting the locus name stored in df.columns
        l = len(df.columns)
        x = [int(j) for j in list(df.index)]

        suptitle_fontsize = 10
        fig_ax = plt.subplots(3,3)  # returns fig, axis_array
        fig_ax[0].suptitle("Flank length optimization", fontsize=suptitle_fontsize)

        for c in zip(df.columns[0:l:2],df.columns[1:l:2]):
            i_df += 1
            if c[1].startswith('AMEL'):
                continue
            y1 = [int(v) if v else 0 for v in df[c[0]]]
            y2 = [int(v) if v else 0 for v in df[c[1]]]
            ax = fig_ax[1].flat[i_ax]
            current_STR = df.columns[i_df*2][:-2]
            color = {'d':'red', 'u':'blue'}
            flank = {'d':'flank_down_len', 'u':'flank_up_len'}
            ax.plot(x, y1, 'o-', color=color[c[0][-1:]], markersize=2, alpha=.5, label=c[0][-1:])
            ax.axvline(str_info[current_STR][flank[c[0][-1:]]], color=color[c[0][-1:]], linewidth=.8, alpha=.4)
            ax.set_xlim(12,47)
            ax.plot(x, y2, 'o-', color=color[c[1][-1:]], markersize=2, alpha=.5, label=c[1][-1:])
            ax.axvline(str_info[current_STR][flank[c[1][-1:]]], color=color[c[1][-1:]], linewidth=.8, alpha=.4)
            ax.set_xlim(12,47)
            if (i_ax == 0):
                ax.legend(fontsize=5).get_frame().set_lw(0.2)
                #lg.draw_frame(False)
            ax.set_title(current_STR, fontsize=6, fontweight="bold", va="center")
            [t.set_fontsize(axis_label_fontSize) for t in ax.get_xticklabels()]
            [t.set_fontsize(axis_label_fontSize) for t in ax.get_yticklabels()]
            i_ax += 1
        fig_ax[0].savefig(os.path.join(outdir, 'flank_opt_results.pdf'))







### 


class FileOperations:
    """ This class makes sure that the neccessary files are opened and
        closed properly, and it also writes and plots the results to those files.
    """

    def __init__(self, outdir, num_STRs):
        self.outdir = outdir

        ### output files: per uniqe flanking AND STR sequences
        self.outputFormat1 = '{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t{10}\t{11}\t{12}\t{13}\t{14}\t{15}\t{16}\n'
        self.resultsFile_seq_count = open( os.path.join(outdir,'STR_seq_count.results'), 'w' )  # sorted according to # sequences; highest count first
        #(n_errors_fu, err_type_fu, err_pos_fu, rs_fu, n_errors_fd, err_type_fd, err_pos_fd, rs_fd)
        header = self.outputFormat1.format('BarcodeID','SampleName','Locus','Length','Allele','PredAllele','ExactParse','#','total','%','AlleleFrq','[neu,etu,epu,rsu,ned,etd,epd,rsd]','rs','Flank_Up','Nom/Seq','Flank_Down','ReportedNom')
        self.resultsFile_seq_count.write( header )

        ### output files: per uniqe STR sequence, irrespective of flanking sequences
        self.outputFormat2 = '{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t{10}\t{11}\t{12}\t{13}\n'
        # ignore mismatches in flanks + add total # of that locus & % of that allele at that locus columns
        self.resultsFile_seq_count_noFlanks = open( os.path.join(outdir,'STR_seq_count_noFlanks.results'), 'w' )
        header = self.outputFormat2.format('BarcodeID','SampleName','Locus','Length','Allele','PredAllele','ExactParse','#','total','%','AlleleFrq','Flank_Up_ref','Nom/Seq','Flank_Down_ref')
        self.resultsFile_seq_count_noFlanks.write( header )


        ### Summary results file
        self.outputFormat3 = '{0}\t{1}\t{2}\t{3}\n'
        self.resultsFile_STR_total_count = open( os.path.join(outdir,'STR_total_count.results'), 'w' )  # total # seqs for each allele
        header = self.outputFormat3.format('Locus','Allele','#','%')
        self.resultsFile_STR_total_count.write( header )


        ### output files: allele prediction results
        self.outputFormat4 = '{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\n'
        self.resultsFile_predictedAlleleResults_wFlanks = open( os.path.join(outdir,'STR_alleleResultsPrediction_wFlanks.results'), 'w' )
        header = self.outputFormat4.format('Locus', 'Allele', 'Counts', 'Tot.Coverage', 'FlankUp','Sequence','FlankDown', 'AlleleFrq')
        self.resultsFile_predictedAlleleResults_wFlanks.write( header )

        # ignore mismatches in flanks
        self.outputFormat5 = '{0}\t{1}\t{2}\t{3}\t{4}\t{5}\n'
        self.resultsFile_predictedAlleleResults_woFlanks = open( os.path.join(outdir,'STR_alleleResultsPrediction_woFlanks.results'), 'w' )
        header = self.outputFormat5.format('Locus', 'Allele', 'Counts', 'Tot.Coverage', 'Sequence', 'AlleleFrq')
        self.resultsFile_predictedAlleleResults_woFlanks.write( header )


        ### Plots
        self.pp = PdfPages(os.path.join(outdir,"alleles_{0}.pdf".format(os.path.basename(self.outdir))))
        
        self.num_STRs = num_STRs   # TODO: how many STRs do we have: not used yet; should b used to determine hw many subplots to create
        self.subplot_n = -1   # current subplot number; incremented when plotting results for a new STR; used for keeping track of the next plot in the figure

        ## the plots will be plotted in a grid of plots_y * plots_x plot positions
        plots_y = 6 #3
        plots_x = 6 #4

        self.title_fontsize = 5

        suptitle_fontsize = 10
        self.fig_ax_1 = plt.subplots(plots_y,plots_x)  # returns fig, axis_array
        self.fig_ax_1[0].suptitle("All alleles with frq > 1% (including flank mismatches)", fontsize=suptitle_fontsize)
        self.fig_ax_2 = plt.subplots(plots_y,plots_x)
        self.fig_ax_2[0].suptitle("All alleles with frq > 1% (ignoring flank mismatches)", fontsize=suptitle_fontsize)
        self.fig_ax_3 = plt.subplots(plots_y,plots_x)
        self.fig_ax_3[0].suptitle("Predicted alleles w flanks", fontsize=suptitle_fontsize)
        self.fig_ax_4 = plt.subplots(plots_y,plots_x)
        self.fig_ax_4[0].suptitle("Predicted alleles wo flanks", fontsize=suptitle_fontsize)
        self.all_fig_ax = [self.fig_ax_1, self.fig_ax_2, self.fig_ax_3, self.fig_ax_4]


    def __enter__(self):
        return self


    def writeResultFiles(self, STR, str_info, sample_name, barcode_id, str_profile, str_profile_noflanks):
        flank_up = str_info[ STR ]["flank_up"]
        flank_up_rev = str_info[ STR ]["flank_up_rev"]
        flank_down = str_info[ STR ]["flank_down"]
        flank_down_rev = str_info[ STR ]["flank_down_rev"]

        n_matches = sum([ int(s[1]['count']) for s in str_profile.iteritems()])
        #allele_counts = dict(Counter([ s[1]['allele'] for s in str_profile.iteritems() ])) # TODO: This does not do what I want ... 

        str_profile_filtered = filter(lambda s: int(s[1]['count'])/float(n_matches)*100 > 1.0, str_profile.iteritems())
        str_profile_filtered_no_flanks = filter(lambda s: int(s[1]['count'])/float(n_matches)*100 > 1.0, str_profile_noflanks.iteritems())

        #for p in str_profile.iteritems():
            

        self.subplot_n += 1  # subplot number

        ### if none left, exit function
        if ( len(str_profile_filtered) == 0 ):
            print("none left")
            return

        ### Results taking flank mismatches into account
        # get a list sorted according to # of occurences of the allele
        sorted_profile = sorted( str_profile_filtered, key=lambda x : x[1]['count'], reverse=True )
        # plot a bar charts of the alleles and their frequencies
        self._plotAlleleProfile(self.fig_ax_1, sorted_profile, STR)

        predicted_allele_results_w_flanks = filter(lambda s: s[1]['predicted_allele'] != '', str_profile.iteritems())
        max_allele_count = sorted_profile[0][1]['count']

        for s in sorted_profile:
            ## get SNP info
            # This is what the 'flank_indels' entry contains:
            # n_errors_fu = s[1]['flank_indels'][0]
            # rs_fu = s[1]['flank_indels'][3] if n_errors_fu else ['']
            # err_type_fu = s[1]['flank_indels'][1]
            # err_pos_fu = s[1]['flank_indels'][2]
            # n_errors_fd = s[1]['flank_indels'][4]
            # rs_fd = s[1]['flank_indels'][7] if n_errors_fd else ['']
            # err_type_fd = s[1]['flank_indels'][5]
            # err_pos_fd = s[1]['flank_indels'][6]
            
            # I assume that there's only one rs ID per flank.
            rsu = s[1]['flank_indels'][3][0] if s[1]['flank_indels'][3][0] or not str_info[ STR ]['rsu'] else str_info[ STR ]['rsu'].split(',')[2] + '[' + str_info[ STR ]['rsu'].split(',')[3] + ']'
            rsd = s[1]['flank_indels'][7][0] if s[1]['flank_indels'][7][0] or not str_info[ STR ]['rsd'] else str_info[ STR ]['rsd'].split(',')[2] + '[' + str_info[ STR ]['rsd'].split(',')[3] + ']'
            print(s[1]['flank_indels'][3])
            print('RSU = ', rsu)
            print('RSD = ', rsd)
            rs_report = rsu + rsd

            ## ReportedNom entry in the output file
            reported_nom = (
                STR +
                '[' + s[1]['allele'] + ']' +
                s[1]['nomenclature'] +
                rs_report
            )

            ## Write to file
            self.resultsFile_seq_count.write(self.outputFormat1.format(
                barcode_id,
                sample_name,
                STR, # locus
                len(s[0][1]), # length of STR
                s[1]['allele'],  # allele
                s[1]['predicted_allele'],  # predicted allele?
                s[1]['exactly_parsed'], 
                s[1]['count'],  # count
                n_matches, # total # of this STR locus
                math.ceil( int(s[1]['count'])/float(n_matches)*100*10 )/float(10),  # % of allele including STR SNPs (1 decimal)
                int(s[1]['count'])/float(max_allele_count),  # #/max(#)
                #(n_errors_fu, err_type_fu, err_pos_fu, rs_fu, n_errors_fd, err_type_fd, err_pos_fd, rs_fd)
                s[1]['flank_indels'],
                rs_report,  # rs IDs, if any
                s[0][0],    # flank_up
                s[1]['nomenclature'], #if s[1]['nomenclature'] else s[0][1].lower(),  # nomenclature or STR seq
                s[0][2], # flank_down
                reported_nom
            )) 

        ## write FASTA files for this STR system
        with open(os.path.join(self.outdir,STR + '_woFlanks.fasta'),'w') as seq_woFlanks_fasta:
            with open(os.path.join(self.outdir,STR + '_wFlanks.fasta'),'w') as seq_wFlanks_fasta:
                total_seq_counter = 0  # a sequence ID
                for s in sorted_profile:  # these are sorted according to count
                    #for i in xrange(int(s[1]['count'])):  # i is an allele ID
                        #total_seq_counter += 1
                    fasta_id = \
                        '>' + STR + \
                        ' Allele:' + str(s[1]['allele']) + \
                        ' Count:' + str(s[1]['count']) + '\n' #\
                        #' ID:' + str(i+1) + '_' + str(total_seq_counter) + '\n'
                    seq_woFlanks_fasta.write(fasta_id)
                    seq_woFlanks_fasta.write(s[0][1] + '\n')
                    seq_wFlanks_fasta.write( fasta_id )
                    seq_wFlanks_fasta.write(''.join(s[0]) + '\n')


        ### Results disregarding mismatches in flanking regions.
        # get a list sorted according to # of occurences of the allele
        sorted_profile = sorted( str_profile_filtered_no_flanks, key=lambda x : int(x[1]['count']), reverse=True )
        # plot a bar charts of the alleles and their frequencies
        self._plotAlleleProfile(self.fig_ax_2, sorted_profile, STR)

        predicted_allele_results_wo_flanks = filter(lambda s: s[1]['predicted_allele'] != '', str_profile_noflanks.iteritems())
        max_allele_count = sorted_profile[0][1]['count']

        # print to file
        for s in sorted_profile:
            self.resultsFile_seq_count_noFlanks.write(self.outputFormat2.format(
                barcode_id,
                sample_name,
                STR, # locus
                len(s[0][1]), # length of STR
                s[1]['allele'],
                s[1]['predicted_allele'],
                s[1]['exactly_parsed'], 
                s[1]['count'],
                n_matches, # total # of this STR locus
                int(s[1]['count'])/float(n_matches)*100,  # % of allele including STR SNPs
                int(s[1]['count'])/float(max_allele_count),  # #/max(#)
                flank_up,
                s[1]['nomenclature'], #if s[1][3] else s[0].lower(),  # nomenclature or STR seq
                flank_down)) # STR

        # plot a bar charts of the predicted alleles and their frequencies
        if ( len(predicted_allele_results_w_flanks) > 0 ):
            self._plotAlleleProfile(self.fig_ax_3, predicted_allele_results_w_flanks, STR)

        # plot a bar charts of the predicted alleles and their frequencies
        if ( len(predicted_allele_results_wo_flanks) > 0 ):
            self._plotAlleleProfile(self.fig_ax_4, predicted_allele_results_wo_flanks, STR)

        ### total # of sequences for each allele
        #sorted_alleles = sorted( allele_counts.items(), key=lambda x : x[1], reverse=True )
        #for s in sorted_alleles:
        #    self.resultsFile_STR_total_count.write(self.outputFormat3.format(
        #        STR,
        #        s[0],
        #        s[1],
        #        s[1]/float(n_matches)*100 ))  # % of allele disregarding STR SNPs
        #self.resultsFile_STR_total_count.write( '{0} total: {1}\n\n'.format(STR,n_matches) )

        # Write a file with all alleles and # of occurences (not just those that >1%)
        sorted_profile = sorted( str_profile.iteritems(), key=lambda x : x[1]['count'], reverse=True )
        for s in sorted_profile:
            self.resultsFile_STR_total_count.write(self.outputFormat3.format(
                STR,
                s[1]['allele'],
                s[1]['count'],
                s[1]['count']/float(n_matches)*100 ))  # % of allele disregarding STR SNPs
        self.resultsFile_STR_total_count.write( '{0} total: {1}\n\n'.format(STR,n_matches) )


    def _plotAlleleProfile(self, fig, sorted_profile, STR, annotate=False):
        """ Given a figure, fig, a sorted_profile dictionary (with or without SNP info) and a string, STR,
        plot a barplot of the frequencies of the alleles.
        """
        a = [float(s[1]['allele']) for s in sorted_profile]
        x = np.array(a)
        c = [int(s[1]['count']) for s in sorted_profile]
        y = np.array(c)

        # draw AMELX and AMELY on the same axis
        amels = {"AMELX","AMELY"}
        other_amel = amels ^ {STR}
        other_amel = other_amel.pop()
        if (STR in amels):
            plot_title = "AMELX/Y"

            amel_index = [s[0] for s in enumerate(fig[1].flat) if s[1].get_title() == plot_title]
            amel_index = amel_index[0] if len(amel_index) > 0 else None

            if amel_index:
                ax = fig[1].flat[amel_index]
            else:
                ax = fig[1].flat[self.subplot_n]

            if STR == "AMELX":
                x = np.zeros_like(y)
            else:
                x = np.ones_like(y)
        else:
            if DEBUG:
                print(STR)
                print(self.subplot_n)
                print(fig[1].flat)
            ax = fig[1].flat[self.subplot_n]
            plot_title = STR

        bar_width = .2
        bar_color = '.05' #  dark gray
        bar_alpha = .5
        ax.bar(x, y, width=bar_width, linewidth=0, color=bar_color, align='center', alpha=bar_alpha)  # no edges
        ax.set_title(plot_title, fontsize=self.title_fontsize, fontweight="bold", va="top")

        if ( annotate ):
            ### annotate the barplot
            try: # get SNPs into a list of pairs (up flank SNP, down flank SNP)
                rs = [(s[1][4][3],s[1][4][7]) for s in sorted_profile]
            except: # There's no info in the _noFlanks version of sorted_profile
                rs = [('','') for _ in sorted_profile]

            # get STR allele nomenclature field into a list
            nom = [s[1][3] for s in sorted_profile]

            # annotate bars with nomenclature and SNP info
            for i,j,n,s in zip(a,c,nom,rs):
                nom_snp = n + '-' + s[0] + ',' + s[1] if s != ('','') else n
                ax.text(i,j,nom_snp, fontsize=1)


        ### set axis ranges
        x_min_range = 7  # minimum x axis range to plot (to avoid a single wide bar on the plot)
        x_range = max(x) - min(x)  # find actual range
        x_pad = x_min_range - x_range if x_range < x_min_range else 0  # if < minimum, pad it to be = minimum
        x_min = np.floor(min(x) - x_pad / 2.0)
        x_max = np.ceil(max(x) + x_pad / 2.0)
        ax.set_xlim(x_min,x_max)

        ### axis ticks
        ax.xaxis.set_ticks_position('none')
        ax.yaxis.set_ticks_position('left')
        ax.set_xticks(np.unique(x))

        ### axis labels
        def toAlleleCount(x,pos):
            return str(STR_Allele(x))

        axis_label_fontSize = 4
        if STR not in amels:
            ax.xaxis.set_major_formatter(ticker.FuncFormatter(toAlleleCount))
        else:
            ax.xaxis.set_major_locator(ticker.FixedLocator([0,1]))
            ax.xaxis.set_major_formatter(ticker.FixedFormatter(["AMELX","AMELY"]))

        plt.setp(ax.get_xticklabels(), fontsize=axis_label_fontSize, rotation=45)
        [t.set_fontsize(axis_label_fontSize) for t in ax.get_yticklabels()]


    def _closeFiles(self):
        self.resultsFile_seq_count.close()
        self.resultsFile_STR_total_count.close()
        self.resultsFile_predictedAlleleResults_wFlanks.close()
        self.resultsFile_predictedAlleleResults_woFlanks.close()

        # adjust all y axes, so they show the same scale
        #print(self.all_fig_ax)
        y_max = max( zip( *[ax.get_yaxis().get_view_interval() for ax in
            itertools.chain.from_iterable(
                [list(a[1].flat) for a in self.all_fig_ax]) ] )[1] )
        [ax.set_ylim(0, y_max) for ax in
            itertools.chain.from_iterable(
                    [list(a[1].flat) for a in self.all_fig_ax]) ]

        def set_proper_axes(fig_ax):
            for ax in fig_ax.flat:
                ### remove all axes (the box) and add only the x and y axes
                ax.set_frame_on(False)
                ax.set_axis_off()
                if ax.get_title(): # if the axis doesn't have a title, don't draw it
                    ax.set_axis_on()
                    xmin, xmax = ax.get_xaxis().get_view_interval()
                    ymin, ymax = ax.get_yaxis().get_view_interval()
                    ax.add_artist(plt.Line2D((xmin,xmax), (ymin,ymin), color="black", linewidth=2))
                    ax.add_artist(plt.Line2D((xmin,xmin), (ymin,ymax), color="black", linewidth=2))

        [set_proper_axes(a[1]) for a in self.all_fig_ax]
        [self.pp.savefig(a[0]) for a in self.all_fig_ax]  # render figure on the pdf backend canvas
        self.pp.close()


    def __exit__(self, exc_type, exc_value, exc_tb):
        # __exit__(type,value,traceback)  # see return value of sys.exc_info
        self._closeFiles()

        if exc_type is None:
            print('exited normally\n')
        else:
            print('raise an exception! ' + str(exc_type))
            return False  # propagate exception



def findSTRRegions(seq_count_file_name, str_filename, str_info, STR, flank_mism, flank_length_up=None, flank_length_down=None):
    """ Given a FASTQ file name, an STR name that is represented in the
        str_info data structure, and a number of flank_mism,
        find all sequences that cover the STR region in the FASTQ file,
        and store them in a file.
        Look for both the flanking sequence and their reverse complement.
        Return the file name of the resulting file.
    """

    # get flanking regions for this STR locus into convenient var names
    flank_up = str_info[ STR ]["flank_up"]
    flank_up_rev = str_info[ STR ]["flank_up_rev"]
    flank_down = str_info[ STR ]["flank_down"]
    flank_down_rev = str_info[ STR ]["flank_down_rev"]

    if DEBUG:
        print("FU:{0} \nFD:{1} \nFUR:{2} \nFDR:{3}".format( flank_up, flank_down, flank_up_rev, flank_down_rev ))

    def grepForFlanks(flank_up, flank_down, rev_compl=False, append=False):
        """ agrep for all sequences that contain both constant flanking regions,
            allowing for 'flank_mism' indels/substitutions
        """
        ud_mism = str_info[ STR ]["mismatches"]
        u_mism = flank_mism if ud_mism == None else ud_mism[0]
        d_mism = flank_mism if ud_mism == None else ud_mism[1]
        # [ref](http://wiki.christophchamp.com/index.php/Agrep)
        # the default agrep mismatch penalties are -D1 -I1 -S1
        # --show-position: result is in the form '0-12:AATGACATTTAACTTGAGAAGAAG...'
        #   for a match from char #0 to char #11 (0 based just like in Python)
        cmd =   'agrep --show-position -' + str(d_mism) + ' -i ' + flank_down + ' ' + seq_count_file_name + \
                ' | agrep --show-position -' + str(u_mism) + ' -i ' + flank_up + \
                ' | awk -v OFS=\":\" \'{print $0,\"' + ('1' if rev_compl else '0') + '\"}\'' + \
                (' >> ' if append else ' > ') + str_filename
        #print(cmd)
        os.system( cmd )

    grepForFlanks(flank_up, flank_down)
    grepForFlanks(flank_up_rev, flank_down_rev, rev_compl=True, append=True)



def parseStrFile(STR_filename, str_info, STR, vcf_file):
    """
    """
    with open( STR_filename, 'r' ) as STR_file: # open resulting file
        ### prepare dictionaries that will hold the results. One dictionary with matching flanks and one dictionary with reference flanks.
        # ---------------------------------------------
        # 'strProfile' is a dict with key
        #   (upstream flanking region, STR region, downstream flanking region) = (str,str,str)
        # and value containing dictionaries with the following keys
        #   allele: str
        #   predicted_allele: str
        #   count: int
        #   rev_compl: bool
        #   nomenclature: str
        #   flank_indels: (int,int,int,int): and the # of flanking region insertions and deletions (fu_ins,fu_del,fd_ins,fd_del)
        # ------------------------------------------------
        str_profile = {}
        str_profile_noflanks = {}
        # Dict cache to keep track of positions that have already been looked up in SNPdb.
        # It saves time not having to look up the same position more than once.
        vcf_searches = defaultdict( str )
        line_n = 0  # keep track of the line number in the file for DEBUG purposes

        for s in STR_file:
            if DEBUG: print(s)
            line_n += 1

            # get start and end pos (as strings) of flank up and down, sequence, seq count,
            # and whether the seq was found using the rev compl of flanks
            # A line is of the form: "u1-u2:d1-d2:seq:seq_count:rev_compl", where rev_compl is 0 (forward) or 1 (reverse compl)
            (u1,u2,d1,d2,seq,seq_count,rev_compl) = sum([x.split('-') for x in s.strip().upper().split(':')],[])
            rev_compl = (rev_compl == "1")

            # it can happen that the up flank is at the beg. of the seq and is missing 1 base (for 1 mismatch) => matches ':'
            u1 = int(u1) - len(d1+d2) - 2  # 2 is for '-' and ':' in 'd1-d2:seq'
            u1 = 0 if ( u1 < 0 ) else u1
            u2 = int(u2) - len(d1+d2) - 2
            d1 = int(d1)
            d2 = int(d2)

            if DEBUG: print("a")
            flank_up_match = strtools.revCompl( seq[d1:d2] ) if rev_compl else seq[u1:u2]
            str_match = strtools.revCompl( seq[u2:d1] ) if rev_compl else seq[u2:d1]
            flank_down_match = strtools.revCompl( seq[u1:u2] ) if rev_compl else seq[d1:d2]

            flank_up = str_info[ STR ]["flank_up"]
            flank_up_rev = str_info[ STR ]["flank_up_rev"]
            flank_down = str_info[ STR ]["flank_down"]
            flank_down_rev = str_info[ STR ]["flank_down_rev"]
            chrom = str_info[STR]["chrom"]
            start = str_info[STR]["start"]  # pos of first base in STR seq on the fwd strand
            end = str_info[STR]["end"]  # pos of the first base after the STR seq on the fwd strand

            if DEBUG:
                print("revCompl = {0}".format(rev_compl))
                print("seq up = {0}".format(seq[u1:u2]))
                print("seq down = {0}".format(seq[d1:d2]))
                print("flank_up_match = {0}".format(flank_up_match))
                print("flank_down_match = {0}".format(flank_down_match))
                print("flank_up ref = {0}".format(flank_up))
                print("flank_up_rev ref = {0}".format(flank_up_rev))
                print("flank_down ref = {0}".format(flank_down))
                print("flank_down_rev ref = {0}".format(flank_down_rev))
                print("str_match = {0}".format(str_match))

            # TODO: Y does this happen for some? Just by chance?
            if DEBUG:
                if len(str_match) == 0:
                    print("No STR sequence found between flanks.")

                    if u1 <= d1 <= u2:
                        print("u1 <= d1 <= u2")

                    print(" Skipping to next sequence.")
                    continue


            ### Store results in two ways:
            def storeResults(profile, profile_key):
                """ Store a single result in dictionary. If str nomenclature result already exists then count one up.

                    param profile : dictionary for entering results, eg str_profile
                    param profile_key : triple consisting of upstream flanking region, STR region and downstream flanking region
                """
                profile[profile_key] = profile.get( profile_key, {} ) #defaultdict( lambda : str() )
                profile[profile_key]['count'] = profile[profile_key].get('count',0) + int(seq_count)
                profile[profile_key]['rev_compl'] = rev_compl

            if DEBUG: print("b")
            profile_key_flanks = (flank_up_match,str_match,flank_down_match)
            storeResults(str_profile, profile_key_flanks)
            if DEBUG: print("c")
            profile_key_no_flanks = (str_info[ STR ]["flank_up"],str_match,str_info[ STR ]["flank_down"])
            storeResults(str_profile_noflanks, profile_key_no_flanks)
            if DEBUG: print("d")


            def getFlankDiffs(flank,flank_match):
                """ Right now it only deals with *at most 1* mismatch.
                    ids == 1 iff ins in fm (= flank_match)
                    ids == -1 iff del from fm
                    ids == 0 iff subst in fm
                    ids == -2 iff they're the same

                    flank : reference sequence
                    flank_match : matched sequence
                """
                ## insertion, deletion, substitution data struct
                ids_data = {
                    "diffs": 0,  # # diffs
                    "fm_ids": [],  # list of indel/subst in fm
                    "fm_ids_pos": []  # the positions of the indel/subst in fm
                }
                if DEBUG: print("ca")
                if (flank != flank_match):  # not the same => an indel or a subst
                    #alignments = pairwise2.align.globalxx(flank,flank_match)
                    if DEBUG:
                        print("cb")
                        print(flank)
                        print(flank_match)
                    alignments = pairwise2.align.localms(flank,flank_match,2,-1,-4,-1)
                    if DEBUG: print("cc")
                    top_aln = alignments[0]
                    if DEBUG: print("cd")
                    f,fm,score,st,en = top_aln  # flank, flank_match, score, start, end
                    if DEBUG: print("ce")
                    fm = fm.rstrip('-')  # trim all '-' from the end of fm

                    #if STR == 'D5S818':
                    #print( pairwise2.format_alignment(*top_aln) )
                    #    print(fm)

                    #fm_ids_pos = -2  # flank_match indels position
                    #ids = -2 #ins = fm_del = fm_sub = -1
                    #diffs = 0
                    if DEBUG: print("cf")
                    # Check for mismatches found.
                    # * Count total number of mismatches
                    # * Register type and adjust position. Position is later used for lookup in vcf info.
                    #   -1 : deletion, pos -1
                    #    0 : substitution, same pos
                    #   +1 : insertion, pos -1
                    for i,(fb,fmb) in enumerate(zip(f,fm)):  # flank base, flank_match base
                        if (fb == '-'): # insertion in fm
                            ids_data['diffs'] += 1
                            ids_data['fm_ids_pos'].append( i-1 )
                            ids_data['fm_ids'].append( 1 )
                        elif (fmb == '-'): # deletion from fm
                            ids_data['diffs'] += 1
                            ids_data['fm_ids_pos'].append( i-1 )
                            ids_data['fm_ids'].append( -1 )
                        elif (fb != fmb): # substitution
                            ids_data['diffs'] += 1
                            ids_data['fm_ids_pos'].append( i )
                            ids_data['fm_ids'].append( 0 )
                        else:
                            pass
                    
                    # TODO : remove?     
                    if ids_data['diffs'] > 2:
                        if DEBUG:
                            #if STR == 'D5S818':
                            
                            print("diffs = ", ids_data['diffs'])
                            print("flank:", flank)
                            #if DEBUG:
                            print("flank_match", flank_match)
                            #print(s)
                            #print(seq)
                            #print(str_match)
                            print("Hmmm.. doesn't do what I want.")
                            #raise Exception()
                    #else:
                    #    if STR == 'D5S818':
                    #        print("ids", ids)
                    #        print("pos", fm_ids_pos)
               
                return ids_data

            def get_SNP_from_VCF(chrom, pos, vcf_file):
                """ Given a 0-based genomic position (chrom,pos) and a VCF file path,
                    returns an rs number for a variation if it's found in the VCF file.

                    get_SNP_from_VCF(chrom,pos) --> None
                    get_SNP_from_VCF(chrom,pos) --> rsNumber
                """
                if DEBUG: print("ba")
                if not vcf_file:
                    return None

                #print("Searching for SNP in " + vcf_file)
                if DEBUG: print("bb")
                # genomic positions in a VCF file are 1-based
                vcf_pos = pos + 1  
                #print vcf_pos-1, vcf_pos, vcf_pos+1
                #cmd = 'egrep -m 1 "' + chrom + '\s+' + str(vcf_pos) + '\s+" ' + vcf_file + ' | cut -f3'
                # ignore the 'chr'
                chrom = chrom[3:]  
                if DEBUG: print( "Searching for SNPs at " + chrom + ':'+str(vcf_pos) )
                cmd = 'vcf-query ' + vcf_file + ' -r ' + chrom+':'+str(vcf_pos)+'-'+str(vcf_pos) + " -f '%LINE\\n' | awk 'length($4)==1 && length($5)==1 {print $0}' | cut -f3"
                #print cmd
                (status, rs) = commands.getstatusoutput(cmd)
                if status:
                    print( "error" )
                    sys.exit(1)
                else:
                    if rs:
                        print( 'Found rs:\n\t{0}'.format(rs) )
                    return(rs)


            ### find the positions of flank indels and SNPs
            if DEBUG: print("e")
            

            def get_snp_info(flank, flank_match, is_flank_up = True):
                """ input: ref flank and flank_match
                    output: (# diffs, [ids], [chrom positions], [rs IDs])
                """
                #print("FU") if is_flank_up else print("FD")
                if DEBUG: print("aa")
                ids_data = getFlankDiffs(flank,flank_match)
                poss = ids_data['fm_ids_pos']  # positions within flank_match where there is a diff
                chrom_poss = []  # chr:pos for each flank diff
                rss = [] if len(poss) > 0 else ['']  # rs id's

                #print(ids_data['diffs'])

                ## for each pos where there's a flank diff
                if DEBUG: print("ab")
                for pos in poss:
                    ## calc actual genomic pos of flank diff
                    if str_info[ STR ]["strand"] == 'f':
                        if is_flank_up:
                            pos = start - len(flank) + pos if pos != -2 else pos
                        else:
                            pos = end + pos if pos != -2 else pos
                    else:
                        if is_flank_up:
                            pos = end + (len(flank) - 1) - pos if pos != -2 else pos
                        else:
                            pos = start - pos - 1 if pos != -2 else pos


                    # Store position for searching in vcf data. Vcf data is 1-based as opposed to the 0-based exisisting position
                    chrom_pos = chrom + ":" + str(pos+1)
                    chrom_poss.append( chrom_pos )
                    rs = ''

                    # Look up positions in vcf info - nomnclature file if vcf data exist there, else supplied vcf file
                    if DEBUG: print("ac")
                    #print("vcf_file =", vcf_file)
                    #print(bool(vcf_file))
                    if vcf_file:  # look for SNPs in dbSNP if the snp_info list is empty
                        #print("vcf")
                        #sys.exit()
                        if chrom_pos not in vcf_searches:
                            rs = get_SNP_from_VCF(chrom,pos,vcf_file) if pos > -2 else ''
                            vcf_searches[chrom_pos] = rs
                        else:
                            rs = vcf_searches[chrom_pos]
                    else:  # look only for those SNPs specified in the snp_info list of SNPdb entries
                        #print("snp_info")
                        print("ad")

                        snp_info = [str_info[ STR ]["rsu"], str_info[ STR ]["rsd"]]
                        snp_info = [snp for snp in snp_info if snp != None]

                        print(".-", snp_info)

                        for snp in snp_info:
                            print( "..", STR, snp)
                            snp_pos = snp.split(',')[0] + ':' + snp.split(',')[1]  # chr:pos
                            snp_rs = snp.split(',')[2]
                            if DEBUG: print("ae")
                            # Compare positions, ignore 'chr' in chrom_pos
                            if chrom_pos[3:] == snp_pos:
                                #print("Found: ", snp_pos)
                                #if str_info[ STR ]["strand"] == 'f':
                                rs = snp_rs + '[' + snp.split(',')[4] + ']'
                                print("rs: ", rs)
                                #else:
                                #    rs = snp_rs + '[' + strtools.revCompl( snp.split(',')[4] ) + ']'
                                #print("RS: ", rs)
                                break  # there cannot b more than one SNP at the same position

                    if DEBUG: print("af")
                    rss.append(rs)  # if an rs ID was found, add it, otherwise just add ''
                    #print("RS_final: ", rs)
                    #print("RSS: ", rss)
                    #print(ids_data['fm_ids'])

                #if not str_info[STR]["rs"] in rss:  # if none found, insert the reference allele SNP name (this assumes that there's only one per flank so far)
                #    rs = str_info[STR]["rs"][0] if is_flank_up else str_info[STR]["rs"][1]
                #    rss.append( rs )
                    #sys.exit(0)
                    if DEBUG: print("ag")

                return (ids_data['diffs'], ids_data['fm_ids'], chrom_poss, rss)

            # Get SNP information for both flanks
            if DEBUG: print("f")
            diffs_u, idss_u, chrom_poss_u, rss_u = get_snp_info(flank_up, flank_up_match)
            if DEBUG: print("g")
            diffs_d, idss_d, chrom_poss_d, rss_d = get_snp_info(flank_down, flank_down_match, is_flank_up = False)

            if DEBUG: print("h")
            # add flank mismatch info to strProfile
            str_profile[profile_key_flanks]['flank_indels'] = (
                diffs_u,idss_u,chrom_poss_u,rss_u,
                diffs_d,idss_d,chrom_poss_d,rss_d )

            if DEBUG: print("i")
        ### add match pattern if there's a nomenclature match
        if DEBUG: print("A")
        fsa = strtools.getFSA( str_info[ STR ]["nomenclature"], str_info[ STR ]["unit_len"], str_info[ STR ]["allele_subtract"], str_info[STR]["allele_formula"] )

        if DEBUG: print("B")

        def storeNomAndAllele(profile):
            for sp in profile:
                if DEBUG: print(sp)
                sequence = sp[1] #if str_info[ STR ]["strand"] == 'f' else strtools.revCompl(sp[1])
                (accept,match_pat,allele_num) = fsa(sequence)
                if DEBUG: print("-")
                profile[sp]['nomenclature'] = match_pat
                profile[sp]['allele'] = str(allele_num)
                profile[sp]['predicted_allele'] = ""
                profile[sp]['exactly_parsed'] = accept

        storeNomAndAllele(str_profile)
        if DEBUG: print("C")
        storeNomAndAllele(str_profile_noflanks)

    ## Predict alleles: the function will add to the 'predicted_allele' entry
    ## in the given dictionary.
    predictAlleleResults(str_profile)
    predictAlleleResults(str_profile_noflanks)

    return (str_profile,str_profile_noflanks)



def predictAlleleResults(str_profile_dict):
    """ !It modifies the input dictionary by adding the predicted alleles.
        Returns nothing.
    """
    min_count = 10
    n_matches = sum([ int(s[1]['count']) for s in str_profile_dict.iteritems()])
    filtered_profile = filter(lambda s: int(s[1]['count']) > min_count and int(s[1]['count'])/float(n_matches)*100 > 1.0, str_profile_dict.iteritems())
    sorted_minCount = sorted( filtered_profile, key=lambda x : x[1]['count'], reverse=True )

    if len(sorted_minCount) >= 2: # take the top 2
        allele_1 = sorted_minCount[0]
        allele_2 = sorted_minCount[1]
        allele_frq = float(allele_2[1]['count']) / float(allele_1[1]['count'])

        if (allele_frq <= float(1)/5):  # frq <= 1/5: only one allele, the other is probably a stutter
            #allele_2 = allele_1
            str_profile_dict[allele_1[0]]['predicted_allele'] = allele_1[1]['allele']
        elif allele_frq < float(1)/3:  # 1/5 < frq < 1/3: grey zone: right now I print them both TBD
            str_profile_dict[allele_1[0]]['predicted_allele'] = allele_1[1]['allele']
            str_profile_dict[allele_2[0]]['predicted_allele'] = allele_2[1]['allele']
        else: # frq >= 1/3: both alleles are real
            str_profile_dict[allele_1[0]]['predicted_allele'] = allele_1[1]['allele']
            str_profile_dict[allele_2[0]]['predicted_allele'] = allele_2[1]['allele']

    elif len(sorted_minCount) == 1: # homozygous
        allele_1 = allele_2 = sorted_minCount[0]
        str_profile_dict[allele_1[0]]['predicted_allele'] = allele_1[1]['allele']
    else:  # no candidates
        pass



def toUniqSeqCountFileFromFastq( data_file, outdir, basename ):
    """ Given a FASTQ file 'data_file'
        generate a text file 'seq_count_file_name' that contains lines of the form
            seq:count
        where seq is the sequence and count is the number of times
        that particular sequence occurs in 'data_file'
    """
    seq_count_file_name = os.path.join(outdir, basename)
    seq_count_file_name += '.sc'
    seq_count_file_dict = {
        basename : {"file_name" : seq_count_file_name, "barcode_id" : basename } }
    if os.path.isfile(seq_count_file_name): return seq_count_file_dict

    seqs = defaultdict(int)
    with open(data_file) as fq:
        for line in fq:
            if line.startswith("@"):
                line = fq.next().strip()
                if not line.startswith("@"):
                    seqs[line] += 1
                    fq.next()
                    fq.next()
                else:
                    print( "FASTQ parese error." )
                    sys.exit(2)

    with open(seq_count_file_name,'w') as sc:
        for s in seqs:
            sc.write(s+":"+str(seqs[s])+"\n")

    return seq_count_file_dict



def toUniqSeqCountFileFromBam( data_file, outdir, basename ):
    print( "Porcessing", data_file )
    #bam_file = BedTool(data_file)
    # open BAM file for reading (a binary file)
    bam_file = pysam.Samfile(data_file, "rb")

    # get read groups and sample names from header
    # header is a dict
    # the 'RG' entry is a list of dicts
    # TODO: we initialize the necessary dictionaries so that 
    rg_seqs = defaultdict(dict)
    sample_names = defaultdict(str)
    seq_count_file_info = defaultdict(dict)

    print( "Finding RGs:" )
    for rg in bam_file.header['RG']:  # for each entry in the list of dicts
        # TODO: consider changing this so that it's clear for the user what type of info is needed in the BAM file and what the exact syntax of that info should b
        print( "Sample Name: " + rg['SM'] )
        match = re.search(".*\\.(.*)", rg['ID'])
        if match:
            rg_ID = match.group(1)
        else:
            rg_ID = rg['ID']
        print( "RG: " + rg_ID )
        rg_seqs[rg_ID] = defaultdict(int)
        sample_names[rg_ID] = rg['SM'].replace(' ', '_')  # RG ID to Sample Name mapping; replace evt. spaces with underscores

    # create dirs and sec count file names
    for rg_id in rg_seqs.keys():
        seq_count_file_name = \
            basename + "_" + rg_id + "_" + sample_names[rg_id] + '.sc'
        new_out_dir = outdir

        if len(rg_seqs) > 1:  # if there's more than one sample, make a subdir per sample
            new_out_dir = os.path.join(outdir, rg_id + '_' + sample_names[rg_id])
            if not os.path.exists(new_out_dir):
                os.makedirs( new_out_dir )

        seq_count_file_name = os.path.join(new_out_dir,seq_count_file_name)
        # info dict indexed by Sample Name
        seq_count_file_info[sample_names[rg_id]] = \
            {   "file_name" : seq_count_file_name,
                "barcode_id" : rg_id }

    # if they all already exists, just return the info dict
    if all([ os.path.isfile(sn_fn_rg[1]['file_name']) for sn_fn_rg in
        seq_count_file_info.items() ]):
        return seq_count_file_info

    # process lines in BAM file
    # print out how many lines have been processed so far.
    i = 0
    for read in bam_file.fetch():
        i += 1
        rg_id = [t[1] for t in read.tags if t[0] == 'RG'][0]
        match = re.search(".*\\.(.*)", rg_id)
        if match:
            rg_ID = match.group(1)
        else:
            rg_ID = rg_id
        # count up hw many times this particular seq has been found
        rg_seqs[rg_ID][read.seq] += 1
        if i % 100000 == 0: print( "Processed {0} lines".format(i) )
        #if i == 500000: break  # Test: read only the first 500000 lines

    # write sequences n their occurence counts to files
    for rg_id in rg_seqs.keys():
        print(sample_names[rg_id])
        with open(seq_count_file_info[sample_names[rg_id]]['file_name'],'w') as sc:
            for s in rg_seqs[rg_id]:
                sc.write(s+":"+str(rg_seqs[rg_id][s])+"\n")

    return seq_count_file_info



def worker_process_STR(more_args):
    """ Determine result file name, start search for STR regions and then start parsing STR regions.
    """
    STR = more_args[0]
    outdir = more_args[1]['outdir']
    str_info = more_args[1]['str_info']
    seq_count_file_name = more_args[1]['seq_count_file_name']
    flank_mism = more_args[1]['flank_mism']
    vcf_file = more_args[1]['vcf_file']

    ### get all STR regions with proper flanks into a file
    STR_filename = os.path.join(outdir, STR + '.str')   # output matching sequences to a file named by the STR name
    if ( not os.path.isfile(STR_filename) ):
        log(log_file, 'Processing flanks for ' + STR)
        findSTRRegions(seq_count_file_name, STR_filename, str_info, STR, flank_mism)
    else:
        log(log_file, 'Not processing flanks for ' + STR + ' (file already exists)')

    ### Parse STRs from file. Returns parsing results in dictionaries. See the fct def for details.
    log(log_file, 'Parsing results file: ' + STR_filename)
    (str_profile,str_profile_noflanks) = parseStrFile(STR_filename, str_info, STR, vcf_file)

    ### how many sequences did we consider for this STR locus?
    nMatches = sum([ int(s[1]['count']) for s in str_profile.iteritems()])
    log(log_file, 'Finished processing {}, {} matches.'.format(STR,nMatches))
    return (STR, str_profile, str_profile_noflanks)



def processSeqCountFile(basename, str_info, str_order, sample_name, barcode_id, seq_count_file_name, flank_mism, vcf_file, n_processes):
    """ Analyze STR systems in parallel given a seq_count_file if intermediary results does not exist in a pickle file. Write intermediary results to pickle file and write result files.
    """
    log(log_file, 'Processing ' + seq_count_file_name)
    outdir = os.path.dirname(seq_count_file_name)

    ### Parse fasta files and write result files
    # we pickle the parsing results so that we do not have to re-parse if we change the output files/plots
    pickle_file_name = os.path.join(outdir, basename+'.pickle')

    # A dict, indexed by STR, that for each STR holds the analysis results data structures (see below)
    result_data = {}

    # TODO: inform the user that the analysis won't b re-run if a pickle file already exists
    if not os.path.isfile( pickle_file_name ):
        pool = mp.Pool(processes=n_processes)
        more_args = {
            'outdir' : outdir,
            'str_info' : str_info,
            'seq_count_file_name' : seq_count_file_name,
            'flank_mism' : flank_mism,
            'vcf_file' : vcf_file
        }
        results = pool.map(worker_process_STR, itertools.izip(str_order, itertools.repeat(more_args)))

        ### Store results in defaultdict
        for r in results:
            STR = r[0]
            result_data[STR] = {}
            result_data[STR]['sample_name'] = sample_name
            result_data[STR]['barcode_id'] = barcode_id
            result_data[STR]['str_profile'] = r[1]
            result_data[STR]['str_profile_noflanks'] = r[2]

        with open(pickle_file_name, "wb") as pickleFile:
            try: # pickle results data
                pickle.dump( result_data, pickleFile )
            except pickle.PicklingError as e:
                print( e )
                sys.exit(2)

        pool.close()
        pool.join()
    else:
        with open(pickle_file_name, "rb") as pickleFile:
            result_data = pickle.load(pickleFile)

    ### Write results to files
    with FileOperations(outdir, len(str_info)) as outputFilesObject:
        log(log_file, 'Generating results files')
        for STR in str_order:
            str_profile = result_data[STR]['str_profile']
            if len(str_profile) != 0:
                outputFilesObject.writeResultFiles(STR, str_info, **result_data[STR])

    ### Print # seqs processed
    seq_used =sum([
        sum([ s[1]['count'] for s in rd_str[1]['str_profile'].iteritems() ])
        for rd_str in result_data.iteritems() ])
    log(log_file, str(seq_used) + ' sequences used')


############



def main():
    startupMessage()

    ## Process command line parameters
    (data_file, str_nom_file, genome_index, flank_mism, outdir, vcf_file, n_processes, max_f_len) = processCommandLineParams()

    ## Initiate log file and write command line parameters.
    # TODO: perhaps change time format so it can b sorted more easily
    global log_file
    log_file = os.path.join(outdir, 'log_' + time.asctime().replace(' ', '_').replace(':', '-') + '.log')
    log(log_file, "Analysis date: " + time.asctime(), append = False)
    log_params = {
        'outdir' : outdir,
        'data_file' : data_file,
        'str_nom_file' : str_nom_file,
        'genome_index' : genome_index,
        'flnk_mism' : flank_mism,
        'SNPdb_file' : vcf_file,
        'n_processes' : n_processes
    }
    log_run_params(log_file, log_params)

    ## Convert fastq/BAM to unique sequence counts
    print( 'Converting FASTQ/BAM to sequence counts' )
    (basename, ext) = os.path.splitext( os.path.basename(data_file) )
    if (ext.lower() == '.fq' or ext.lower() == '.fastq'):
        seq_count_file_info = toUniqSeqCountFileFromFastq( data_file, outdir, basename )
    elif (ext.lower() == '.bam'):
        seq_count_file_info = toUniqSeqCountFileFromBam( data_file, outdir, basename )
    else:
        print( "Unknown extension:", ext )
        print( "Must be either a FASTQ or a BAM file." )
        sys.exit(2)

    ## build STR info data structure
    #if ( max_f_len ):
    #    str_info, str_order = strtools.strNomFileReader( str_nom_file, genome_index, flank_up_len_set=max_f_len, flank_down_len_set=max_f_len )
    #else:
    str_info, str_order = strtools.strNomFileReader( str_nom_file, genome_index )
    #if len(snp_info) == 0:
    #    log(log_file, 'As there is SNP info specified in the .sn file, the specified VCF file won\'t be used.')

    #sys.exit(0)

    ## flank length stats: see hw many we find w a variety of flank lengths
    if (max_f_len):
        ## run on rnd sample from current run
        #file_index = np.random.randint(len(seq_count_file_names))
        #seq_count_file_name = seq_count_file_names[file_index]
        file_index = np.random.randint(len(seq_count_file_info.items()))
        sn_fn_rg = seq_count_file_info.items()[file_index]
        seq_count_file_name = sn_fn_rg[1]['file_name']
        print("Flank optimization on: {}".format(seq_count_file_name))
        flank_length_optimization( 14, max_f_len, seq_count_file_name, str_info, flank_mism, n_processes, True )
        sys.exit(0)

        #for seq_count_file_name in seq_count_file_names:
        #    flank_length_optimization( 14, max_f_len, seq_count_file_name, str_info, flank_mism, n_processes, True )
        #    sys.exit(0)  # temporarily only run optimization on one sample from each run

    ## process each file, sorted according to file name
    for sn_fn_rg in sorted( seq_count_file_info.items(), key = lambda x: x[1]['file_name'] ):
        sample_name = sn_fn_rg[0]
        barcode_id = sn_fn_rg[1]['barcode_id']
        seq_count_file_name = sn_fn_rg[1]['file_name']

        #if sample_name == "87465":
        processSeqCountFile(basename, str_info, str_order, sample_name, barcode_id, seq_count_file_name, flank_mism, vcf_file, n_processes)
        #sys.exit(0)  # only run on the first sample

    log(log_file, "End time: " + time.asctime())



if __name__ == '__main__':
    print(dir())
    main()






### TEST of 'regex module which alowes for searching for a regex w. mismatches'
    # if (False):
    #     with open(seq_count_file_name) as f:
    #         res = []
    #         matches = 0
    #         for l in f:
    #             for s in str_order:
    #                 fu = str_info[s]["flank_up"]
    #                 fur = str_info[s]["flank_up_rev"]
    #                 fd = str_info[s]["flank_down"]
    #                 fdr = str_info[s]["flank_down_rev"]
    #                 pat = "("+fu+"){e<=1}(.*)("+fd+"){e<=1}" + "|" + "("+fur+"){e<=1}(.*)("+fdr+"){e<=1}"
    #                 m = regex.findall(pat,l)
    #                 if m:
    #                     #res.extend(m)
    #                     matches += int(l.strip().split(':')[1])
    #         print matches
    #     sys.exit(0)